package org.poc.mappingservice;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.poc.mappingservice.entities.Manager;
import org.poc.mappingservice.repositories.ManagerRepository;

import java.util.Optional;

@ExtendWith(MockitoExtension.class)
@Slf4j
public class ManagerTest {
    @Mock
    private ManagerRepository managerRepository;
    @Test
    void getManagerTest() {
        Optional<Manager> manager = managerRepository.findByEmployee_Profession("Ingineer");
        manager.ifPresent(value -> log.info(value.toString()));
    }
}
