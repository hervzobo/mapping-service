package org.poc.mappingservice;

import lombok.Builder;
import lombok.extern.slf4j.Slf4j;
import org.poc.mappingservice.entities.Manager;
import org.poc.mappingservice.repositories.ManagerRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.Optional;

@SpringBootApplication
@Slf4j
public class MappingServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(MappingServiceApplication.class, args);
	}
	@Bean
	CommandLineRunner start(ManagerRepository managerRepository) {
		return args -> {
			Optional<Manager> manager = managerRepository.findByEmployee_Profession("Ingineer");
			manager.ifPresent(value -> log.info(value.toString()));
		};
	}
}
